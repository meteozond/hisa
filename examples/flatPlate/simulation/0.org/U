/*--------------------------------*- C++ -*----------------------------------*\
|                                                                             |
|    HiSA: High Speed Aerodynamic solver                                      |
|    Copyright (C) 2014-2017 Johan Heyns - CSIR, South Africa                 |
|    Copyright (C) 2014-2017 Oliver Oxtoby - CSIR, South Africa               |
|                                                                             |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       volVectorField;
    location    "0";
    object      U;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#include        "include/freestreamConditions"

dimensions      [0 1 -1 0 0 0 0];

internalField   uniform $U;

boundaryField
{
    inlet
    {
        type            characteristicFarfieldVelocity;
        #include        "include/freestreamConditions"
        value           uniform $U;
    }
    freestream
    {
        type            characteristicFarfieldVelocity;
        #include        "include/freestreamConditions"
        value           uniform $U;
    }
    inviscidWall
    {
        type            slip;
    }
    viscousWall
    {
        type            fixedValue;
        value           uniform (0 0 0);
    }
    outlet
    {
        type            zeroGradient;
    }
}


// ************************************************************************* //
